# vnord
vnord.net source code

This is just a basic hakyll site. I'm making the source code available anyway, so that you can take a look at the source of the posts or whatever-the-hell, and perhaps it will become more relevant in the future if I change the site structure more.

To build, you need to have hakyll installed. Install haskell and cabal-install first (DuckDuckGo is your friend), then run cabal-install hakyll. Then build the site with ghc --make site.hs. The site files will then be available in the _site folder.
