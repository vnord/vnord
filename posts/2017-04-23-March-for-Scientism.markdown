---
title: March for Scientism
---

The 'March for "Science"' has got to be one of this year's most amusing spectacles. The only thing that differentiates its hilarity from the sort of hilarity evoked by jokes about the disabled, is that these people are fully, completely, 100% serious. And the lack of self-awareness forces us to describe it in such terms as 'tragically comical'.

Thanks to [slate](http://www.slate.com/blogs/the_slatest/2017/04/22/here_are_some_of_the_best_signs_from_the_march_for_science.html), I did not have to look far for for a rich selection of tragicomedy:

![](https://i.imgur.com/tBFIzcu.jpg "No, I can't believe I'm embedding this image on my website, either"){ width=100% }

![](https://i.imgur.com/AZEC7AW.jpg "No comment"){ width=100% }

![](https://i.imgur.com/4YUr7TM.jpg "If you fail to appreciate the humour, perhaps you haven't read enough Moldbug"){ width=100% }


Now obviously, I'm not being entirely fair. I only picked the funniest photos from the article (there are some signs that are genuinely clever and not as awful as the rest). And perhaps you won't share my meta-amusement over the fact that more and less advanced organisms can look at the same selection of messages and derive such vastly different sentiments from them. So to make up for that, here's a dessert from the march's [twitter feed](https://twitter.com/sciencemarchdc). (The tweet itself has, unfortunately, been deleted (why?). Much more gold is to be found there, but this really tops it.)

![](https://i.imgur.com/20DFWZ4.png "No, it was absolutely not a joke")

Right, that's enough for today. Quite enough.
