---
title: Hello, vnord!
---

If you are reading this, it follows naturally that you must be an immensely powerful, and probably also somewhat evil, time-traveller, come here in order to correct some errors in the time-space-flux caused by the contents of this early 2000s 'webpage'. I regret to inform you that the task is not merely formidable - it is, in fact and despite your immense powers, impossible. Because, you see, the information found here has been carefully crafted to incur semantical paradoxes that will deprecate time-travellers. Stand by and observe.
