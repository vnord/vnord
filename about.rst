---
title: About
---

vnord.net is my personal vnordlog, which I intermittently and spontaneously update with my thoughts on various matters that interest me, like language, social culture, computer science, literature, film, music, theology and various types of intellectual philosophy. My words and opinions should be read as spurious fiction and should therefore of course always be taken at face value (fnord).

The source code can be found here_.

.. _here: https://bitbucket.org/vnord/vnord/ 
