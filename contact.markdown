---
title: Contact
---

If you wish to initiate a two-way (or even-more-way) conversation with yours truly, it may be advantageous for you to employ some electronic mail protocols and compose a message to <admin@vnord.net>. Admin is a mysterious figure who lives in Gothenburg, Sweden, and who loves to have strangers over for tea in his cozy little late-20s flat - but not complete strangers, so send him first a message and introduce yourself, and he shall proceed to inform you of his more precise coordinates. If you like admin prefer sending real, paper letters, a similar procedure may be followed.
